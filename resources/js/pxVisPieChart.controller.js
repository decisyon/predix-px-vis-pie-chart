(function() {
	'use strict';

	/*
	 * Controller to manage the pie chart widget.
	 * link: https://www.predix-ui.com/?show=px-vis-pie-chart&type=component
	 *
	*/
	function PxVisPieChartCtrl($scope, $timeout, $element) {
		var ctrl						= this,
			ctx                         = $scope.DECISYON.target.content.ctx,
			refObjId                    = ctx.instanceReference.value,
			target                      = $scope.DECISYON.target,
			unbindWatches               = [],
			DEFAULT_DISABLE             = 'false',
			OVERFLOW_WIDTH_BUG_FIX      = 10,
			OVERFLOW_HEIGHT_BUG_FIX     = 120;

		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			unbindWatches.push($scope.$watch('DECISYON.target.content.ctx', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)){
					ctrl.manageParamsFromContext(newCtx);
				}
			}, true));
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};

		/* Return the size input after adjusting its value by removing % and space white.
		The width and the height are adjusted because the Predix component doesn't supported values in percentage*/
		/* It adjust size ad fix overflow*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace(/%/g, '').trim();
			return (parseInt(adjustedSize) !== '') ?	parseInt(adjustedSize)	:	'';
		};
		
		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
			Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/
		var applyDimensionsToWebComponent = function(ctx) {
			var height	=	getAdjustedSizing(ctx['min-height'].value),
				width = getAdjustedSizing(ctx['min-width'].value);
			ctrl.height	=	(height !== '' && height	>	OVERFLOW_HEIGHT_BUG_FIX)	?	(height - OVERFLOW_HEIGHT_BUG_FIX) : height;
			ctrl.width	=	(width !== '' && width	>	OVERFLOW_WIDTH_BUG_FIX)	?	(width - OVERFLOW_WIDTH_BUG_FIX)	: width;
		};

		/* Set the callback for data connector*/
		var setCallbackDC = function(dataConnector) {
			dataConnector.instance.data.then(function success(data) {
				ctrl.updateData(data);
			},function error(rejection) {
				setChartInError(rejection);
			});
		};

		/* Function to handle DC failure */
		var setChartInError = function(rejection) {
			ctrl.errorMsg = rejection.errorMsg;
			ctrl.currentTpl = 'pxVisPieChartInError.html';
			destroyWidget();
		};

		/* Destroy the widget reference*/
		var destroyWidget = function() {
			var i = 0;
			for (i; i < unbindWatches.length; i++) {
				unbindWatches[i]();
			}
		};
		/*fetch and set values to respective properties from manageParamsFromContext */
		ctrl.getCustomPropertyValue = function(propertry, defaultValue) {
			if (propertry.value !== '') {
				return propertry.value;
			} else {
				return defaultValue;
			}
		};

		/* Management of pie chart properties */
		ctrl.manageParamsFromContext = function(context) {
			var num = 0;
			ctrl.donut				=	ctrl.getCustomPropertyValue(context.$pxPCDonut,	DEFAULT_DISABLE);
			ctrl.showTitle			=	ctrl.getCustomPropertyValue(context.$pxPCShowTitle,	DEFAULT_DISABLE);
			ctrl.usePercentage		=	ctrl.getCustomPropertyValue(context.$pxPCUsePercentage,	DEFAULT_DISABLE);
			ctrl.preserveDataorder	=	ctrl.getCustomPropertyValue(context.$pxPCPreserveDataOrder,	DEFAULT_DISABLE);
			ctrl.aggregateORegister	=	ctrl.getCustomPropertyValue(context.$pxPCAggregateOtherRegister,	DEFAULT_DISABLE);
			ctrl.maximumRegister	=	parseInt(ctrl.getCustomPropertyValue(context.$pxPCMaximumRegister,	num));
			ctrl.innerRadius		=	parseInt(ctrl.getCustomPropertyValue(context.$pxPCInnerRadius.value,	num));
			ctrl.decimalPercentage	=	parseInt(ctrl.getCustomPropertyValue(context.$pxPCDecimalPercentage,	num));
		};

		/* Set specific properties to Predix component */
		ctrl.setAttributesToWidget = function() {
			var visPieChart = document.querySelector('#' + ctrl.widgetID);
			ctrl.setBooleanAttributeOnlyIsTrue('donut', ctrl.donut, visPieChart);
			ctrl.setBooleanAttributeOnlyIsTrue('show-title', ctrl.showTitle, visPieChart);
			ctrl.setBooleanAttributeOnlyIsTrue('use-percentage', ctrl.usePercentage, visPieChart);
			ctrl.setBooleanAttributeOnlyIsTrue('preserve-data-order', ctrl.preserveDataorder, visPieChart);
			ctrl.setBooleanAttributeOnlyIsTrue('aggregate-other-register', ctrl.aggregateORegister, visPieChart);
			ctrl.setBooleanAttributeOnlyIsTrue('preserve-data-order', ctrl.preserveDataorder, visPieChart);
			ctrl.setNumericAttribute('max-registers', ctrl.maximumRegister, visPieChart);
			if (ctrl.usePercentage === true) {
				ctrl.setNumericAttribute('decimal-Percentage', ctrl.decimalPercentage, visPieChart);
			}
			if ((ctrl.donut === true) && ctrl.innerRadius > 0) {
				visPieChart.setAttribute('inner-radius', (ctrl.innerRadius / 100));
			}
		};

		/* Set the boolean attribute only if true because Predix component doesn't want the attribute in its DOM node if false*/
		ctrl.setBooleanAttributeOnlyIsTrue = function(attribute, boolValue, visPieChart) {
			if (boolValue) {
				visPieChart.setAttribute(attribute, boolValue);
			}
		};

		/* Set the attribute only if value is greater than 0*/
		ctrl.setNumericAttribute = function(attribute, value, visPieChart) {
			if (value > 0) {
				visPieChart.setAttribute(attribute, value);
			}
		};

		/* Populate the data fetched through controller */
		ctrl.updateData = function(data) {
			ctrl.chartData = data;
			applyDimensionsToWebComponent(ctx);
			ctrl.currentTpl = 'pxVisPieChart.html';
		};

		/* Includes functionality corresponding to selected template */
		$scope.$on('$includeContentRequested', function(angularEvent, src) {
			if (src == 'pxVisPieChart.html') {
				$timeout(function() {
					ctrl.setAttributesToWidget();
					ctrl.manageParamsFromContext(ctx);
					attachListeners();
				});
			}
		});

		/* Get data from the Connector */
		ctrl.getData = function(context) {
			var dc;
			ctrl.useDataConnector = context.useDataConnector.value;
			if (ctrl.useDataConnector) {
				if (target.getDataConnector) {
					dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function success(data) {
							setCallbackDC(dataConnector);
						});
						setCallbackDC(dataConnector);
					});
				} else {
					setChartInError({errorMsg : 'The widget can\'t be inizialized because it needs to be associated to a data connector.'});
					ctrl.chartData = [];
				}
			} else {
				setChartaInError({errorMsg : 'The Use data connector property is set to false.'});
			}
		};

		/* Initialize */
		ctrl.inizialize = function() {
			ctrl.widgetID = 'pxPieChart_' + refObjId;
			ctrl.getData(ctx);
			ctrl.manageParamsFromContext(ctx);
			attachListeners();
		};

		ctrl.inizialize();
	}
	PxVisPieChartCtrl.$inject = ['$scope', '$timeout', '$element'];
	DECISYON.ng.register.controller('pxVisPieChartCtrl', PxVisPieChartCtrl);
}());