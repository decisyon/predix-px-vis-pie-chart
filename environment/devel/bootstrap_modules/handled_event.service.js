(function() {
  'use strict';
  
  /**
   * Monica Modena
   * The service DcyHandledEventService manages the HTTP request with handled event.
   * 
   * @params requestParams {object} parameters in JSON format
   * 
   * Example of requestParams:
   * - url {string} url path
   * - params.revent {string} event to call
   * - params.rasterCtx {string} context (serialized in JSON)
   * 
   */
  function DcyHandledEventService(dcyHTTPRequestService) {
	  
	  function getResponse(requestParams) {
		  var params = {
				httpRequestType: 'POST',
				subPath: '/cassiopeaWeb/report/empty.jsp.cas',
				params : {
					rasterCtx: requestParams.rasterCtx,
					revent: requestParams.revent,
					csMLO : csMLO
				}
		  };
		  
		return dcyHTTPRequestService(params);
		
	  }

	  return getResponse;
  }
  
  DcyHandledEventService.$inject = ['dcyHTTPRequestService'];

  angular.module('dcyApp.services').service('dcyHandledEventService', DcyHandledEventService);

}());
